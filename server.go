package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	port = "8000"
)

type info struct {
	title string
	date  string
	user  string
}

var (
	validNums = []string{"11111111", "22222222"}
	infos     = []info{}
)

func main() {
	http.HandleFunc("/login", login)
	http.HandleFunc("/user", existUser)
	http.HandleFunc("/select", selectContents)
	http.HandleFunc("/not-exist", notExist)
	http.HandleFunc("/lend", lend)
	http.HandleFunc("/complete", complete)
	log.Println("DEBUG: listen on", port)
	http.ListenAndServe(":"+port, nil)
}

func login(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "login.html")
}

func existUser(w http.ResponseWriter, r *http.Request) {
	n := r.URL.Query().Get("num")
	isExist := false
	for _, v := range validNums {
		if n == v {
			isExist = true
			break
		}
	}
	if isExist {
		http.Redirect(w, r, "/select?num="+n, http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/not-exist?num="+n, http.StatusSeeOther)
	}
}

func notExist(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "not-exist.html")
}

func selectContents(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "select.html")
}

func lend(w http.ResponseWriter, r *http.Request) {
	// ParseFormはここでやらないとうまくいかなかった
	r.ParseForm()
	go appendInfo(r)
	http.Redirect(w, r, "/complete", http.StatusSeeOther)
}

func appendInfo(r *http.Request) {
	num := r.Form.Get("num")
	f := r.PostForm
	t := time.Now()
	d := fmt.Sprintf(
		"%04d/%02d/%02d_%02d:%02d",
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(),
	)

	for _, v := range f {
		b := v[0]
		if b == "" {
			continue
		}
		infos = append(infos, info{title: b, date: d, user: num})
	}

	fmt.Print("lent books: ")
	for _, v := range infos {
		fmt.Print(v)
	}
	fmt.Println("")
}

func complete(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "complete.html")
}
